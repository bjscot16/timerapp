//
//  ViewController.swift
//  Timer
//
//  Created by Brendan Scott on 9/16/19.
//  Copyright © 2019 Saint Mary's University. All rights reserved.
//

import UIKit
import CoreData
import Foundation

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // Button setup
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var onOffButton: UIButton!
    
    // Time setup
    @IBOutlet weak var timerLabel: UILabel!
    var timer = Timer()
    var isRunning = false
    var runStartDateTime = Date()
    var totSeconds = 0
    
    // Core Data
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var historyView: UITableView!
    @IBOutlet weak var userDescription: UITextField!
    
    // Arrays for table view
    var cellDescription: [String] = []
    var cellDate: [String] = []
    var cellTime: [String] = []
    
    // View load function
    override func viewDidLoad() {
        fetchData()
        super.viewDidLoad()
        historyView.delegate = self
        historyView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellDescription.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = historyView.dequeueReusableCell(withIdentifier: "savedTimeInfo", for: indexPath)
        
        cell.textLabel?.text = cellDescription[indexPath.row] + " " + cellTime[indexPath.row] + " " + cellDate[indexPath.row]
        
        return cell
    }
    
    /*
     saveTime() saves the time information into core data Entity "History"
     
        Parameters: none
        
        -Returns: none
     */
    func saveInfo() {
        
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "History", in: context)

        let savedProjectTime = NSManagedObject(entity: entity!, insertInto: context)
        
        let myUserDescription = userDescription.text
        let currentDate = Date()
        
        savedProjectTime.setValue(totSeconds, forKey: "runTime")
        savedProjectTime.setValue(myUserDescription, forKey: "userDescription")
        savedProjectTime.setValue(currentDate, forKey: "dateStamp")
        
        // Save context
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
        
        
    }

    /*
     fetchData() retrieves the data from core data reasigns global arrays of
        cellDescription, cellDate, and cellTime with fetched data
     
        Parameters: none
     
        Return: none
     */
    func fetchData() {
        let context = appDelegate.persistentContainer.viewContext
        // Request
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "History")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                let testVal =  data.value(forKey: "runTime") as? Int?
                
                if testVal != nil {
                    let timeStr = getTimeString(seconds: testVal!!)
                    cellTime.insert(timeStr, at: 0)
                }
            }
            cellDescription = []
            for data in result as! [NSManagedObject] {
                let descStr = data.value(forKey: "userDescription") as? String
                
                if descStr != nil {
                    cellDescription.insert(descStr!, at: 0)
                }
            }
            for data in result as![NSManagedObject] {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM/dd/yyyy"
                let dateStr = dateFormatter.string(from: data.value(forKey: "dateStamp") as! Date)

                cellDate.insert(dateStr, at: 0)
            }
        } catch {
            
            print("Failed")
        }
        
        self.historyView.reloadData()
        
    }
    
    
    /*
    toggleRun() starts the timer if it's off or pauses the timer if it's on
     
    -Parameter: calling button
     
    -Return: nothing
    */
    @IBAction func toggleRun(_ sender: Any) {
        if !isRunning {
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runTimer), userInfo: nil, repeats: true)
            
            isRunning = true
            onOffButton.backgroundColor = #colorLiteral(red: 0.9463634201, green: 0, blue: 0.02657206021, alpha: 1)
            onOffButton.setTitle("Stop", for: .normal)
            resetButton.isEnabled = false
            runStartDateTime = Date()
        }
        else if isRunning {
            timer.invalidate()
            timer = Timer()
            
            isRunning = false
            onOffButton.backgroundColor = #colorLiteral(red: 0.001272644625, green: 0.615204632, blue: 0.03583529688, alpha: 1)
            onOffButton.setTitle("Start", for: .normal)
            resetButton.isEnabled = true
            saveInfo()
            fetchData()
        }
    }
    
    // Stop timer and reset timerLabel, totSeconds, and isRunning to defaults
    @IBAction func resetTapped(_ sender: Any) {
        timer.invalidate()
        timer = Timer()
        
        timerLabel.text = "00:00:00"
        totSeconds = 0
        isRunning = false
    }
    
    /*
        Turns int value passed into function into a string representing a time in hours, minutes, and seconds
     
        Parameters:
            seconds: Int
     
        Returns:
            timeStr: String
    */
    @objc func getTimeString(seconds: Int) -> String {
        // Declare and initialize string variables for hours, minutes, and seconds passed
        let elapsedSeconds = seconds
        var strHours = ""
        var strMinutes = ""
        var strSeconds = ""
        var timeString = ""
        
        // Calculate constant int values for hours, minutes, and seconds
        let hours = elapsedSeconds / 3600
        let minutes = (elapsedSeconds % 3600) / 60
        let seconds = (elapsedSeconds % 3600) % 60
        
        
        // Decide what to display for hours
        if (hours < 10) {
            strHours = "0\(hours)"
        }
        else {
            strHours = "\(hours)"
        }
        
        // Decide what to display for minutes
        if (minutes < 10) {
            strMinutes = "0\(minutes)"
        }
        else {
            strMinutes = "\(minutes)"
        }
        
        // Decide what to display for seconds
        if (seconds < 10) {
            strSeconds = "0\(seconds)"
        }
        else {
            strSeconds = "\(seconds)"
        }
        
        timeString = strHours + ":" + strMinutes + ":" + strSeconds
        
        return timeString
    }
    
    /*
     runTimer function to update var count, calculate hours, minutes, and seconds
     It then displays the time string in timerLabel in viewController
    */
    @objc func runTimer() {
        var timeString = ""

        totSeconds = getSeconds(runStartDateTime)
        timeString = getTimeString(seconds: totSeconds)

        timerLabel.text = (timeString)
    }
    
    func getSeconds(_ StartDateTime: Date) -> Int{
        let secondsPassed = Int(Date().timeIntervalSince(StartDateTime))
        return secondsPassed
    }
    
    
}

